$(document).ready( function() {
// Tag construct for a tag
var Tag = {
// Default value
	str: "undefined",
	weight: 0,
	index: 0,
	  init : function(str){  // Method which will display type of Animal
	  	this.weight = 1;
	  	this.str = str;
	  },
	  incrementWeight : function(){
	  	this.weight += 1;
	  },
	  printToConsole : function(){
	  	console.log("Value :"+this.str+" with a weight of: "+this.weight);
	  },
	  index : function (int){
	  	this.index = int;
	  },
}
// Construct for tag cloud
var Cloud = {
	tags: [],	// Array of tags
	keys: [], // Array of key values for tags, redundant as the same is stored in a tag too
	totalWeight: 1, // Total weight of the cloud map
	addTag : function(str){
		var query = this.getTag(str);
		var index = 0;
		if(typeof(query) === 'undefined') {
			var tag = Object.create(Tag);
			tag.init(str);
			index = this.tags.length
			tag.index(index);
			this.tags.push(tag);
			this.keys.push(str.toLowerCase());
			this.incrementTotalWeight();
			console.log("added tag: "+tag.str);
		}
		else {
			query.incrementWeight();
			this.incrementTotalWeight();
			console.log("incremented :"+query.str+" "+query.weight);
			index = query.index;
		}
		return index; // Index to the objects html element
	},
	push : function(tag){
		// Adds a tag to array, needs to be created 1st and passed as parameters. 
		// For adding with string, use addtag(str)
		this.keys.push(tag.str);
		this.tags.push(tag);
	},
	// Basic pop, return last and delete from array
	pop : function() {
		this.keys.pop();
		var tag = this.tags.pop();
		this.totalWeight -= tag.weight();
		return tag;
	},
	// returns the last tag
	last : function() {
		// Gets the last obj without deleting
		return this.tags[this.tags.length-1];
	},
	// search based on the str if tag is in the cloud, returns index or -1 if there is no
	search : function(str){
		str = str.toLowerCase();
		// returns the index of a certain string
		return $.inArray(str, this.keys);
	},
	// gets the tag based on the index or query string
	getTag : function(query){
		if(!isNaN(query)) return this.tags[query];
		// Gets the Tag object with string value
		else return this.tags[this.search(query)];
	},
	// Adds to total weight 
	incrementTotalWeight : function(){
		this.totalWeight += 1;
	},
	// Adds to tags weight
	incrementTagWeight : function (query){
		this.getTag(query).incrementWeight();
	},
	sortByWeight : function (){
		this.tags.sort(function(tagA, tagB){
		  a = tagA.weight;
		  b = tagB.weight;
		  if (a < b) {
		    return -1;
		  }
		  if (a > b) {
		    return 1;
		  }
		  return 0;
		})
	},
	// Shufles the arrays up
	shuffle : function () {
	  var currentIndex = this.tags.length, temporaryTag, randomIndex, temporaryKey ;
	  var tags = this.tags;
	  var keys = this.keys;
	  // While there remain elements to shuffle...
	  while (0 !== currentIndex) {
	    // Pick a remaining element...
	    randomIndex = Math.floor(Math.random() * currentIndex);
	    currentIndex -= 1;
	    // And swap it with the current element.
	    temporaryTag = tags[currentIndex];
	    temporaryKey = keys[currentIndex];
	    tags[currentIndex] = tags[randomIndex];
	    keys[currentIndex] = keys[randomIndex];
	    tags[randomIndex] = temporaryTag;
	    keys[randomIndex] = temporaryKey;
	  }
  	return tags;
	},
	tagsToString : function() {
		var str ="";
		for(var i = 0; i<this.tags.length; i += 1){
			str += " "+ this.tags[i].str;
		}
		return str;
	}

};
// Cloud variable
var cloud = Object.create(Cloud);

var UI = {
	contentElement:"",
	height : 0,
	width : 0,
	// Initialize the cloud tag to a specific html element
	init : function(element){

		var elem = $(element);
		this.contentElement =  elem; // Set the HTML element to user for cloud
		this.height = elem.height();
		this.width = elem.width();
		console.log(this.contentElement +" w:"+this.width+" h:"+this.height);

	},
	getPadding : function (weight){
		console.log("relative weight: "+weight/cloud.totalWeight);
		var relativeScale = weight/cloud.totalWeight;
		var y = (this.height * relativeScale) /2;
		var x = (this.width * relativeScale) /2;
		return y+"px "+x+"px "+y+"px "+x+"px ";
	},
	// Print a tag (object) with .str attribute
	printTag : function(tag){
		var size = this.getPadding(tag.weight);
		var templateStart = "<div class='tag'><p class='tag-value' style='padding:"+size+";'>";
		var templateEnd = "</p></div>";
		var html = templateStart + tag.str + templateEnd;
		contentElement = this.contentElement;
		console.log(html);
		if(typeof(contentElement) !== 'undefined') contentElement.append(html);
	},
	// Refresses the tag cloud on addition 
	refreshCloud : function (){
		this.contentElement.empty();
		var totalWeight = cloud.totalWeight;
		var currentTag;
		var shuffledArr = cloud.shuffle();
		for(var i = 0; i<shuffledArr.length; i+=1){
				currentTag = shuffledArr[i];
				this.printTag(currentTag);
				//padding: 1%; 
		}
		console.log(cloud.tagsToString());
	}
}
// Add tag from the input value 
$("#add").click(function (e){
	var input = $("#tag").val()
	if(input.length > 0) {
		var alreadyInCloud = false;
		if(cloud.search(input) >= 0) alreadyInCloud = true;
		var index = cloud.addTag(input);
		if(!alreadyInCloud)	UI.refreshCloud();
		else UI.refreshCloud();
		console.log("added tag with index: "+index);
	} 
	else; // TODO printt error
	console.log(cloud.keys);
})

// Tests
var index = cloud.addTag("Apina");
cloud.addTag("Jepoi");
var testObject = cloud.last();
testObject.printToConsole();
console.log(index);
console.log(cloud.keys);
console.log(cloud.search("Jepoi"));
cloud.incrementTagWeight("jepoi");
console.log(cloud.getTag("Jepoi"));
var UI = Object.create(UI);
UI.init("#tag-cloud");
console.log(UI.contentElement);
UI.printTag(cloud.getTag('Jepoi'));
cloud.sortByWeight();
});